Obverse Import Bundle
=====================

Description
-----------
The Obverse Import Bundle provides a Symfony2 example of using the [Obverse Import Tool][1] to import data 
from an excel spreadsheet into a database table.  This bundle demonstrates the use of Symfony2's excellent
Service Container and uses Dependency Injection to create instances of the Obverse Import and Importer objects.

Requirements
------------
The requirements for this bundle are:

 * PHP version 5.3.2 or higher

And the requirements for [PHPExcel][2] upon which we rely heavily are:

 * PHP extension php_zip enabled *(this extension is needed for Excel 2007 (.xlsx) files)*
 * PHP extension php_xml enabled
 * PHP extension php_gd2 enabled

Install
-------

1.  Install Symfony

        php composer.phar create-project symfony/framework-standard-edition <dir>

2.  Add Obverse Import Bundle and related packages to composer.json

            {
            "require": {
                "obverse/php-excel": "dev-master",
                "obverse/import": "dev-master",
                "obverse/import-bundle": "dev-master"
            },
            "repositories": [
                   {
                       "type": "composer",
                       "url": "http://www.obverse.com/packages"
                   }
               ]
            }

3.  Install Obverse Import Bundle and supporting files

        php composer.phar update

4.  Add the Obverse Import Bundle to app/AppKernel.php

        {
            return array(
                new Obverse\ImportBundle\ObverseImportBundle(),
            );
        }

4.  Follow instructions from [Obverse Import Tool][1] to setup importer(s)

5.  Add importers to vendor/obverse/import-bundle/Resources/config/services.yml following example in [services.yml]

        services:
            obverse_import.import:
                class: OBV\Import
                arguments: [@obverse_import.excel_reader]
                
            obverse_import.excel_reader:
                class: OBV\Import\Helpers\PhpExcelHelper
        
            doctrine.dbal.default.wrapped_connection:
                factory_service: doctrine.dbal.default_connection
                factory_method: getWrappedConnection
                class: PDO
        
            #
            # Importer Services
            #
            obverse_import.importers.contact:
                class: Obverse\ImportBundle\Importers\Contact
                arguments: [@doctrine.dbal.default.wrapped_connection, "%kernel.root_dir%/../vendor/obverse/import-bundle/Obverse/ImportBundle/Resources/files/contact_column_mapping.yml", 'reg_user']

6.  Add the obverse_import configuration key to your app/config/config.yml

        obverse_import:
            importers: {contact: 1}

7.  Finally run the import command from the console

        php app/console obverse:import


[1]:      https://bitbucket.org/malaney/excel_importer
    "Obverse Import Tool"
[2]:    http://phpexcel.codeplex.com/wikipage?title=Requirements&referringTitle=FAQ
"Requirements for PHPExcel"
