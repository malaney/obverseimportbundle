<?php
namespace Obverse\ImportBundle\Importers;

use OBV\Import\Importers\AbstractImporter;
use OBV\Import\Importers\ImporterInterface;

class Contact extends AbstractImporter implements ImporterInterface
{
    public function saveErrors($errors)
    {
        // Do something here ...
        return true;
    }

    public function importRow($row, $errors=array())
    {
        $newUserID = NULL;
        // Insert into db
        if (!empty($row)) {
            try {
                $this->dbh->beginTransaction();

                $row['status'] = 1;
                $row['entered_by'] = "Import Script";
                $newUserID = $this->createContact($row);

                $this->dbh->commit();
            } catch (\PDOException $e) {
                $this->dbh->rollBack();
                throw $e;
            }
        }
        return array('success' => !empty($newUserID), 'userID' => $newUserID, 'errors' => $errors);
    }

    public function validateEmail($email)
    {
        $email = trim($email);
        // see http://fightingforalostcause.net/misc/2006/compare-email-regex.php
        $emailValidatorRegex = '/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i';
        if (!preg_match($emailValidatorRegex, $email)) {
            throw new \InvalidArgumentException('Invalid email [' . $email . ']');
        }
        return array('data' => strtolower($email), 'errors' => array());
    }

    public function createContact($opts=array()) 
    { 
        $defaults = array(
                'fname' => '', 
                'lname' => '', 
                'email' => '',
                'mobile_phone' => '',
                'office_phone' => '',
                'direct_phone' => '',
                'assigned_to' => '',
                'status' => '',
                'display_name' => '',
                'owner_type' => '',
                'entered_by' => '',
                'facebook' => ''
                );

        $opts = array_merge($defaults, $opts);

        $sql = '
            INSERT INTO '. $this->tableName .' (
                    `fname`, 
                    `lname`, 
                    `email`, 
                    `mobile_phone`, 
                    `office_phone`, 
                    `direct_phone`, 
                    `assigned_to`, 
                    `status`,
                    `display_name`,
                    `date_added`,
                    `owner_type`,
                    `facebook`,
                    `skype`,
                    `twitter`,
                    `website`,
                    `comments`,
                    `gender`,
                    `full_address`,
                    `company_name`,
                    `entered_by`,
                    `birthday`,
                    `fax`,
                    `email_alt`
                    ) 
            VALUES (
                    :fname, 
                    :lname, 
                    :email, 
                    :mobile_phone, 
                    :office_phone, 
                    :direct_phone, 
                    :assigned_to, 
                    :status,
                    :display_name,
                    NOW(),
                    :owner_type,
                    :facebook,
                    :entered_by,
                    :skype,
                    :twitter,
                    :website,
                    :comments,
                    :gender,
                    :full_address,
                    :company_name,
                    :entered_by
                    :birthday,
                    :fax,
                    :email_alt
                   )
            ON DUPLICATE KEY UPDATE 
                contact_id=LAST_INSERT_ID(contact_id),
                status=VALUES(status),
                display_name=VALUES(display_name),
                owner_type=VALUES(owner_type),
                country_id=VALUES(country_id)
            ';
        $named_params = $this->_createNamedParams($opts);

        $sth = $this->dbh->prepare($sql);
        if (!$sth->execute($named_params)) {
            throw new RuntimeException('Bad insert ');
        }
        return $this->dbh->lastInsertID();
    }

    private function _createNamedParams($array)
    {
        $result = array();
        foreach ($array as $key => $val) {
            $result[':'.$key] = (!empty($val)) ? $val : '';
        }
        return $result;
    }
}
