<?php

namespace Obverse\ImportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ObverseImportBundle:Default:index.html.twig', array('name' => $name));
    }
}
