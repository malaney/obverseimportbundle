<?php
namespace Obverse\ImportBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Sensio\Bundle\GeneratorBundle\Command\Helper\DialogHelper;

use Obverse\ImportBundle\DependencyInjection\ObverseImportExtension;

use OBV\Import;

class ImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('obverse:import')
        ->setDefinition(array(
            new InputOption('import_type', '', InputOption::VALUE_REQUIRED, 'What type of data are we importing (i.e. contact or listing)'),
            new InputOption('file', '', InputOption::VALUE_REQUIRED, 'Valid spreadsheet file')
        ))
        ->setDescription('import spreadsheet')
        ->setHelp(<<<EOT
The <info>obverse:import</info> command helps you generate spreadsheet data into a db table

REQUIREMENTS:
1) A db connection and table 
2) A spreadsheet containing data you wish to insert
3) A yaml file containing a mapping of spreadsheet columns to database columns.

How could it be improved?
1)  Importer class is doing a lot, maybe refactor?
2)  Wish we could get rid of the PDO helper and use Doctrine DBAL
3)  Unit tests


You supply a spreadsheet and a yaml file describing the mapping of the fields (Sample files are included).  
The script parses your spreadsheet 
EOT
);
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $importers = $this->getContainer()->getParameter('obverse_import.importers');
        $validImportTypes = array_keys($importers);

        $dialog = $this->getDialogHelper();
        $dialog->writeSection($output, 'Welcome to the Obverse Import script');   
        $output->writeln(array(
            '', 
            'This script will help you to import a spreadsheet of contact data into your database',
            '' 
        )); 

        $import_type = $input->getOption('import_type') ?: $validImportTypes[0];
        $import_type = $dialog->askAndValidate(
                    $output, 
                    $dialog->getQuestion('What kind of data are we importing?  Must be one of ['. join($validImportTypes, ', ') .']', $import_type), 
                    array($this, 'validateImportType'), 
                    false, 
                    $import_type);
        $input->setOption('import_type', $import_type);

        $file = $input->getOption('file') ?: dirname(dirname(__FILE__)) . '/Resources/files/contact_list.xls';
        $file = $dialog->askAndValidate(
                    $output, 
                    $dialog->getQuestion('Select a valid spreadsheet file (.xls, .xlsx, .csv)', $file), 
                    array($this, 'validateFile'), 
                    false, 
                    $file);
        $input->setOption('file', $file);
    }
    
    public function validateFile($filename)
    {
        if (is_readable($filename)) {
            return $filename;
        } else {
            throw new \InvalidArgumentException('The filename '. $filename.' name must be valid and readable');
        }
    }

    public function validateImportType($type)
    {

        $importers = $this->getContainer()->getParameter('obverse_import.importers');
        $validImportTypes = array_keys($importers);
        if (!in_array($type, $validImportTypes)) {
            throw new \InvalidArgumentException('Invalid import type '. $type);
        }
        return $type;
    }

    protected function getDialogHelper()
    {
        $dialog = $this->getHelperSet()->get('dialog');
        if (!$dialog || get_class($dialog) !== 'Sensio\Bundle\GeneratorBundle\Command\Helper\DialogHelper') {
            $this->getHelperSet()->set($dialog = new DialogHelper());
        }

        return $dialog;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $import_type = $input->getOption('import_type');
            $filename = $input->getOption('file');
            $importerClass = $this->getContainer()->get('obverse_import.importers.' . $import_type);

            $import = $this->getContainer()->get('obverse_import.import');
            $import->setImporter($importerClass);
            $import->loadSpreadsheet($filename);
            $results = $import->process();
        } catch (\PDOException $e) {
            $dialog = $this->getDialogHelper();
            $pdoMessage = 'Please verify your database settings in app/config/parameters.yml and 
that you have created the proper table for your importer.

For the initial example, see vendor/obverse/import-bundle/Obverse/ImportBundle/Resources/sql/contact.sql';

            $dialog->writeSection($output, 'PDO Error', 'bg=red;fg=white');   
            $output->writeln($pdoMessage . "\n");   
            $output->writeln($e->getMessage() . "\n");
        } catch (\Exception $e) {
            $dialog = $this->getDialogHelper();
            $dialog->writeSection($output, $e->getMessage(), 'bg=red;fg=white');   
        }

        if (!empty($results['errors'])) {
            $dialog = $this->getDialogHelper();
            $dialog->writeSection($output, $results['errors'][0], 'bg=yellow;fg=white');   
        }
        if (!empty($results['totalRows'])) {
            $output->writeln($results['totalRows'] . " row(s) processed in spreadsheet");
            $output->writeln($results['importedRows'] . " row(s) processed successully");
            $output->writeln(count($results['errors']) . " errors");
        }
    }
}
