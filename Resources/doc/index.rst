Installation
============

1.  Start with a base symfony install
    ```bash
        php composer.phar create-project symfony/framework-standard-edition <somedir>
    ```
2.  Add the following to your composer.json
    ```json
        {
            "require": {
                obverse/php-excel: "dev-master",
                obverse/import: "dev-master",
                obverse/import-bundle: "dev-master"
            }
        }
    ```

3.  Update composer to bring in new vendors
    ```bash
        php composer.phar update
    ```

4.  Add the Obverse Bundle to your app/AppKernel.php
    ```php
    {
        return array(
            new Obverse\ImportBundle\ObverseImportBundle(),
        );
    }
    ```

5.  Create an array of Importer services and supporting files.  
    Importers are classes that do the actual work of taking a row of data, validating/filtering it and 
    persisting it.  Your importer class may need to do sql or mongo, may need to validate or filter values.  
    Anything goes.
    - Start by creating a yml mapping file.  See [link] as an example.  Each column in your spreadsheet should 
      be represented under the 'cols' key.  Use the 'table' key to specify the name of your table/datastore.
        - If a field is mandatory (required) then indicate that by adding a 'required:1' under the column, 
          otherwise set required to 0 or omit the required key altogether.
        - If a field needs to be filtered/validated/processed you may specify an array of callbacks that will be
          called on the field.  The callbacks must be valid functions that exist in your importer class.  See example.

    - Create an importer class that implements OBV\Import\Importers\ImporterInterface (See example)
    - Add each importer class to services.yml and app/config/config.yml

6.  Configure the obverse_import bundle in your app/config/config.yml
    ``` yaml
    obverse_import:
        importers: {contact: contact_map_file, listing: listing_map_file}
    ```

7.  Create an importer class that implements OBV\Import\Importers\ImporterInterface and do whatever you 
    need to get data into your table.  Insert data using the insertRow method, create any callbacks you 
    need (i.e. to validate data, lookup values, etc)

8.  Now you may run the command
    ``` bash
        php app/console obverse:import
    ```
    to actually run your import script.
