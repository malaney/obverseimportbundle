<?php
namespace Obverse\ImportBundle\Helpers;

class MockPdo extends \PDO
{
    public function __construct($dsn, $user, $pass)
    {
        $foo = 'bar';
    }
    
    public function setAttribute($attr, $val)
    {
        $foo = 'bar';
        return true;
    }

    public function beginTransaction()
    {
    }

    public function rollback()
    {
    }

    public function commit()
    {
    }

    public function lastInsertID($seqname = NULL)
    {
        return 42;
    }

    public function prepare($statement, $options=NULL)
    {
        return new MockPDOStatement();
    }
}

class MockPDOStatement extends \PDOStatement
{
    public function execute($bound_input_params = NULL)
    {
        $data = array('foo' => 'bar');
        return $data;
    }
}
