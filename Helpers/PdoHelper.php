<?php
// src/Obverse/ImportBundle/Helpers/PdoHelper.php
namespace Obverse\ImportBundle\Helpers;

use Obverse\ImportBundle\Helpers\MockPdo;
/**
 * PdoHelper
 * This class simply prepares a PDO connection object
 * 
 * @package 
 * @version $id$
 * @copyright 2012 Obverse Dev
 * @author Malaney J. Hill <malaney@gmail.com> 
 * @license PHP Version 3.01 {@link http://www.php.net/license/3_01.txt}
 */
class PdoHelper
{
    public $dbh;

    /**
     * __construct 
     * 
     * @param mixed $driver 
     * @param mixed $host 
     * @param mixed $port 
     * @param mixed $name 
     * @param mixed $user 
     * @param mixed $pass 
     * @param mixed $charset 
     * @access public
     * @return void
     */
    public function __construct($driver, 
                                $host, 
                                $port, 
                                $name, 
                                $user, 
                                $pass, 
                                $charset) {

        switch ($driver) {
            default:
            case 'pdo_mysql':
                $driver = 'mysql';
            break;
        }

        $dsn = sprintf('%s:dbname=%s;host=%s;port=%s;charset=%s', $driver, $name, $host, $port, $charset);
        // $dbh = new \PDO($dsn, $user, $pass);
        $dbh = new MockPdo($dsn, $user, $pass);
        $dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $dbh->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        $this->dbh = $dbh;
    }

    /**
     * getDbh 
     * Returns PDO db handle
     *
     * @access public
     * @return PDO
     */
    public function getDbh()
    {
        return $this->dbh;
    }
}
